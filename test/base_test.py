# -*- coding: utf-8 -*-
import unittest
from utils import *
from demo_client import DemoClient

class BaseTest(unittest.TestCase):
    def __init__(self, test):
        unittest.TestCase.__init__(self, test)
        self.config = Configuration("platform.conf")
        self.name = self.config.get("platform")
        clients = {"demo" : DemoClient}
        self.client = clients[self.name](self.config)

    def setUp(self):
        pass

    def tearDown(self):
        pass


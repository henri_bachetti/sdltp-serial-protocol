# -*- coding: utf-8 -*-
import serial, protocol, sdltp
import os, sys, time, shutil
from hashlib import sha1
from utils import *
from struct import pack, unpack

class SdltpClient:

	ID_GET_VERSION = 'V'
	ID_ECHO = '!'
	ID_SLEEP = 'S'
	ID_REBOOT = 'R'
	ID_GETTIME = 'T'
	ID_SETTIME = 'U'

	ret_string = {'A': 'OK', 'R': 'REFUSED', 'B': 'BUSY', 'U': 'Unknown', 'E': 'FAILED'}

	def __init__(self, config):
		print "Initializing sdltp"
		self.config = config
		self.name = self.config.get("platform")
		device = self.config.get("device")
		baudrate = self.config.get("baudrates")[self.name]
		print '/dev/ttyUSB1', baudrate
		self.line = serial.Serial(device, baudrate, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, timeout=1)
		time.sleep(0.35)
		self.protocol = sdltp.Sdltp(self.line, self.config.get("packets")[self.name])

	def version(self):
		query = self.ID_GET_VERSION
		status = self.protocol.write(query)
		buf = self.protocol.read()
#		print bin2hex(buf)
		return unpack('<7s12s', buf[2:])
	
	def echo(self, s):
		query = self.ID_ECHO + s
		status = self.protocol.write(query)
		return self.protocol.read()[2:]
	
	def settime(self, t = None):
		if (t == None):
			t = int(time.time())
		print "t=", t
		query = pack('<cI', self.ID_SETTIME, t)
#		print bin2hex(query)
		status = self.protocol.write(query)
		buf = self.protocol.read()
		return self.ret_string[buf[1:]]
	
	def gettime(self):
		query = self.ID_GETTIME
		status = self.protocol.write(query)
		buf = self.protocol.read()
#		print bin2hex(buf[2:])
		return unpack('<I', buf[2:])
	
	def reboot(self):
		query = self.ID_REBOOT
		status = self.protocol.write(query)
		buf = self.protocol.read()
		return self.ret_string[buf[1:]]

	def sleep(self, t):
		query = pack('<cH', self.ID_SLEEP, t)
		status = self.protocol.write(query)
		buf = self.protocol.read()
		return self.ret_string[buf[1]]

	def write(self, query):
		return self.protocol.write(query)

	def read(self, query):
		return self.protocol.read(query)


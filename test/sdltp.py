
import time, logging
import serial, protocol
from utils import *

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s\t%(message)s')
logger = logging.getLogger('sdltp')
logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

SOH	= chr(0x01)
STX	= chr(0x02)
ETX	= chr(0x03)
ACK	= chr(0x06)
DLE = chr(0x10)
WAK = chr(0x13)
NAK	= chr(0x15)
EOT	= chr(0x04)
ACK_TIMEOUT = 3
CHAR_TIMEOUT = 3
ERROR_MAX = 6

SIMULATE_CHAR_TIMEOUT = 1
SIMULATE_ACK_TIMEOUT = 2
SIMULATE_NAK = 3
SIMULATE_CRC_ERROR = 4

crctbl = (0x0000, 0x1081, 0x2102, 0x3183,
	0x4204, 0x5285, 0x6306, 0x7387,
	0x8408, 0x9489, 0xA50A, 0xB58B,
	0xC60C, 0xD68D, 0xE70E, 0xF78F)

class Sdltp(protocol.Protocol):

	def __init__(self, line, frame_size):
		protocol.Protocol.__init__(self, line)
		self.error_simulation = 0
		self.seq = 0
		self.crc = 0
		self.frame_size = frame_size

	def get_seq(self):
		seq = self.seq
		self.seq += 1
		if self.seq > 0xff:
			self.seq = 0
		return seq

	def add2crc(self, c):
		self.crc = (self.crc >> 4) ^ crctbl[(ord(c) ^ self.crc) & 0x0F];
		self.crc = (self.crc >> 4) ^ crctbl[((ord(c) >> 4) ^ self.crc) & 0x0F];

	def writeSOH(self, n):
		logger.debug('writeSOH: %d' % n)
		seq = self.get_seq()
		for retry in range(3):
			self.writeChar(SOH)
			self.writeChar(chr(seq))
			self.writeChar(chr(n & 0x00FF))
			self.writeChar(chr(n >> 8))
			try:
				if self.waitForACK() == seq:
					return
			except serial.SerialTimeoutException:
				pass
			logger.debug('retry')

	def writeEOT(self):
		logger.debug('writeEOT')
		seq = self.get_seq()
		for retry in range(3):
			self.writeChar(EOT)
			self.writeChar(chr(seq))
			try:
				if self.waitForACK() == seq:
					return
			except serial.SerialTimeoutException:
				pass
			logger.debug('retry')

	def waitForACK(self):
		logger.debug('waiting for ACK')
		while 1:
			c = self.readChar(ACK_TIMEOUT)
			if c == ACK:
				logger.debug('ACK')
				seq = ord(self.readChar(CHAR_TIMEOUT))
				logger.debug('ACK %d' % seq)
				return seq
			elif c == None:
				logger.error('timeout')
				return None
			elif c == NAK:
				logger.error('NAK')
				return None
			elif c == WAK:
				logger.debug('WAK')
			else:
				logger.error('unexpected %s' % ord(c))

	def writeFrame(self, buf):
#		logger.debug('writeFrame: %s(%d)' % (buf, len(buf)))
		size = len(buf)
		seq = self.get_seq()
		for retry in range(3):
			logger.debug('send %d' % seq)
			self.crc = 0
			self.writeChar(STX)
			self.add2crc(chr(seq))
			self.writeChar(chr(seq))
			for cnt in range(size):
				c = buf[cnt]
				if self.error_simulation == SIMULATE_CHAR_TIMEOUT and cnt > (size / 10):
					logger.info('Error simulation: CHAR_TIMEOUT')
					time.sleep(1)
					self.error_simulation = 0;
				if c in (STX, ETX, DLE, SOH, EOT):
					self.add2crc(DLE)
					self.writeChar(DLE)
				self.add2crc(c)
				self.writeChar(c)
			self.add2crc(ETX)
			self.writeChar(ETX)
			if self.error_simulation == SIMULATE_CRC_ERROR:
				logger.info('Error simulation: CRC_ERROR')
				self.crc += 1
				self.error_simulation = 0
			self.writeChar(chr(self.crc & 0x00FF))
			self.writeChar(chr(self.crc >> 8))

			try:
				if self.waitForACK() == seq:
					return
			except serial.SerialTimeoutException:
				logger.error('timeout')
			logger.error('writeFrame: retry')
		raise protocol.ackTimeoutError

	def readSOH(self):
		seq = ord(self.readChar(CHAR_TIMEOUT))
		n = ord(self.readChar(CHAR_TIMEOUT)) + (ord(self.readChar(CHAR_TIMEOUT)) << 8)
		logger.debug('readSOH: %d' % n)
		return n

	def readEOT(self):
		seq = ord(self.readChar(CHAR_TIMEOUT))
		logger.debug('readEOT: %d' % seq)

	def writeACK(self, n):
		self.writeChar(ACK)
		self.writeChar(chr(n))

	def writeNAK(self, n):
		self.writeChar(NAK)
		self.writeChar(chr(n))

	def readFrame(self, timeout = 3):
		buf = ''
		errors = 0
		while 1:
			self.crc = 0
#			logger.debug('crc = %x' % self.crc)
			seq = ord(self.readChar(CHAR_TIMEOUT))
			self.add2crc(chr(seq))
#			logger.debug('crc = %x' % self.crc)
			try:
				while 1:
					c = self.readChar(CHAR_TIMEOUT)
					self.add2crc(c)
#					logger.debug('crc = %x' % self.crc)
					if c == DLE:
						c = self.readChar(CHAR_TIMEOUT)
						self.add2crc(c)
#						logger.debug('crc = %x' % self.crc)
						buf += c
					elif c == ETX:
						logger.debug('ETX')
						crc = ord(self.readChar(CHAR_TIMEOUT)) + (ord(self.readChar(CHAR_TIMEOUT)) << 8)
						if self.crc != crc:
							logger.error('crc error %x instead of %x' %(crc, self.crc))
							self.writeNAK(seq)
							continue
						if self.error_simulation == SIMULATE_NAK:
							logger.info('Error simulation: NAK')
							self.writeNAK(seq)
							self.error_simulation = 0
							continue
						if self.error_simulation == SIMULATE_ACK_TIMEOUT:
							logger.info('Error simulation: ACK omitted')
							self.error_simulation = 0
							continue
						else:
							self.writeACK(seq)
							return buf
					elif c == STX:
						logger.debug('STX')
						self.crc = 0
#						logger.debug('crc = %x' % self.crc)
						buf = ''
						break
					else:
						buf += c
			except serial.SerialTimeoutException:
				errors += 1
				if errors >= ERROR_MAX:
					raise protocol.readTimeoutError
				logger.error('readFrame: retry')

	def write(self, data, callback=None):
		logger.debug('write: sending %s' % bin2hex(data))
		length = len(data)
		if length > self.frame_size:
			npackets = length / self.frame_size
			remainder = length % self.frame_size
			logger.debug('write: sending %d packets, remain %d' % (npackets, remainder))
			if remainder:
				n = npackets + 1
			else:
				n = npackets
			self.writeSOH(n)
			for packet in range(npackets):
				if callback != None:
					callback(packet, n)
				index = packet * self.frame_size
				f = data[index:index + self.frame_size]
				self.writeFrame(f)
			f = data[length - remainder:]
			if callback != None:
				callback(n, n)
			if len(f):
				self.writeFrame(f)
			self.writeEOT()
		else:
			self.writeFrame(data)
		return length

	def read(self, timeout = 3):
		logger.debug('read')
		buf = ''
		c = self.readChar(timeout)
		if c == SOH:
			npackets = self.readSOH()
			for frame in range(npackets):
				c = self.readChar(timeout)
				if c != STX:
					logger.error('read: Unexpected char %x' % ord(c))
				buf += self.readFrame()
			self.readEOT()
		elif c == STX:
			buf = self.readFrame(timeout)
		elif c == EOT:
			self.readEOT()
			logger.error('read: Unexpected EOT')
		else:
			logger.error('read: Unexpected char %x' % ord(c))
#		logger.debug('read: received %s(%d)' % (bin2hex(buf), len(buf)))
		return buf

	def simulate(self, error):
		self.error_simulation = error

# -*- coding: utf-8 -*-
from struct import pack, unpack
import time
import serial
from sdltp_client import SdltpClient
from utils import *

class DemoClient(SdltpClient):

	CMD_LED = 'L'

	def __init__(self, config):
		SdltpClient.__init__(self, config)

	def read_noxcpt(self):
		while (1):
			try :
				buf = self.protocol.read()
				break;
			except serial.SerialTimeoutException:
				pass
		return buf

	def unknown(self):
		query = '1'
		status = self.protocol.write(query)
		buf = self.protocol.read()
		return self.ret_string[buf[1:]]

	def invalid(self):
		len = self.protocol.frame_size
		query = '1' + len * 'a'
		status = self.protocol.write(query)
		buf = self.protocol.read()
		return self.ret_string[buf[1:]]

	def poweroff(self):
		query = self.ID_POWEROFF
		status = self.protocol.write(query)
		buf = self.protocol.read()
		print bin2hex(buf)
		return self.ret_string[buf[1:]]

	def led(self, state):
		query = self.CMD_LED = pack('<cc', self.CMD_LED, '\x01' if state == 1 else '\x00')
		status = self.protocol.write(query)
		buf = self.protocol.read()
		return self.ret_string[buf[1:2]]


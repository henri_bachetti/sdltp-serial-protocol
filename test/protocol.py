
import time, logging
import serial
from utils import bin2hex

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s\t%(message)s')
logger = logging.getLogger('protocol')
logger.setLevel(logging.ERROR)

SOH	= chr(0x01)
STX	= chr(0x02)
ACK	= chr(0x06)
NAK	= chr(0x15)
EOT	= chr(0x04)
CAN	= chr(0x18)
CRCCHR = 'C'

ackTimeoutError = serial.SerialTimeoutException("ACK timeout")
readTimeoutError = serial.SerialTimeoutException("Read timeout")

class Protocol:

	def __init__(self, line):
		self.line = line
		line.flushInput()
		line.flushOutput()

	def readChar(self, timeout):
		for t in range(timeout):
			s = self.line.read()
			logger.debug('<%s' % bin2hex(s))
			if s != '':
				return s
		raise readTimeoutError

	def writeChar(self, s):
		logger.debug('>%s' % bin2hex(s))
		self.line.write(s)

	def write(self, data):
		raise NotImplementedError

	def read(self, data):
		raise NotImplementedError

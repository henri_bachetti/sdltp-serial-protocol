
import string

def bin2hex(s, space = None):
	if space != None:
		return string.join(["%02X" % ord(x) for x in s], space)
	return string.join(["%02X" % ord(x) for x in s], '')

def bcd2str(code, digits):
	pin = ''
	cnt = 0
	for i in range(len(code)):
		pin += chr((ord(code[i]) >> 4) + 0x30)
		cnt += 1
		if cnt == digits:
			break;
		pin += chr((ord(code[i]) & 0x0f) + 0x30)
		cnt += 1
		if cnt == digits:
			break;
	return pin

def list2bin(l):
	s = ''
	for n in l:
		s += chr(n)
	return s

class Configuration:
	def __init__(self, name):
		self.symbols = {}
		execfile(name, self.symbols)
#		for key, value in self.symbols.items():
#			print "%s : %s" % (key, value)

	def get(self, name):
		return self.symbols[name]

# -*- coding: utf-8 -*-
import os, sys, time, shutil
import unittest
from utils import *
from hashlib import sha1
from random import randint
import serial, sdltp
from sdltp_client import SdltpClient
from base_test import BaseTest
from demo_test import DemoTest

class CommonTest(BaseTest):
    def __init__(self, test):
        BaseTest.__init__(self, test)

    def testVersion(self):
        version = self.client.version()
        print "Appli version:", version[0]
        print "Appli build:", version[1]

    def testGetTime(self):
        print self.client.gettime()
        print "t=", int(time.time())

    def testSetTime(self):
        self.client.settime()

    def testSleep(self):
        self.client.sleep(4000)

    def testCharTimeout(self):
        """ Test of the char timeout """
        self.client.protocol.simulate(sdltp.SIMULATE_CHAR_TIMEOUT)
        print self.client.echo("azertyuiopqsdfghjklm")

    def testAckTimeout(self):
        """ Test of the ACK timeout """
        self.client.protocol.simulate(sdltp.SIMULATE_ACK_TIMEOUT)
        print self.client.echo("azertyuiopqsdfghjklm")
    
    def testNak(self):
        """ Test of the NAK """
        self.client.protocol.simulate(sdltp.SIMULATE_NAK)
        print self.client.echo("azertyuiopqsdfghjklm")
    
    def testCrcError(self):
        """ Test of the CRC error """
        self.client.protocol.simulate(sdltp.SIMULATE_CRC_ERROR)
        print self.client.echo("azertyuiopqsdfghjklm")

if __name__ == "__main__":
    unittest.main()


# -*- coding: utf-8 -*-
import serial
import os, sys, time, shutil
import random
from hashlib import sha1, md5
import unittest
from utils import *
from sdltp_client import SdltpClient
from base_test import BaseTest

class DemoTest(BaseTest):

	def __init__(self, test):
		BaseTest.__init__(self, test)

	def testUnknown(self):
		### requete inconnue ###
		self.assertEqual(self.client.unknown(), 'Unknown')

	def testLedOn(self):
		### LED ON ###
		self.assertEqual(self.client.led(1), 'OK')

	def testLedOff(self):
		### LED OFF ###
		self.assertEqual(self.client.led(0), 'OK')

if __name__ == "__main__":
	unittest.main()

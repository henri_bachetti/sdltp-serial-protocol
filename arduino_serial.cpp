
#include "serial_core.h"
#include "serial_driver.h"
#include "sdltp.h"

struct serial_port_config *serialConfig;

static int hardware_serial_setup(char* options, int flags);
static void hardware_serial_txchar(uint8_t c);
static void hardware_serial_start_tx(void);
static void hardware_serial_stop_tx(void);
static int hardware_serial_init(void);
static int software_serial_setup(char* options, int flags);
static void software_serial_txchar(uint8_t c);
static void software_serial_start_tx(void);
static void software_serial_stop_tx(void);
static int software_serial_init(void);

static int hardware_serial_setup(char* options, int flags)
{
  long baud = 9600;
  int bits = 8;
  int parity = 'n';
  int flow = 'n';
  int stops = 1;
  uint8_t config = 0;

  if (options) {
    uart_parse_options(options, &baud, &parity, &bits, &flow, &stops);
  }
  switch (bits) {
    case 6:
      config |= 0x02;
      break;
    case 7:
      config |= 0x04;
      break;
    case 8:
      config |= 0x06;
      break;
  }
  switch (stops) {
    case 1:
      break;
    case 2:
      config |= 0x08;
      break;
  }
  switch (parity) {
    case 'n':
      break;
    case 'e':
      config |= 0x20;
      break;
    case 'o':
      config |= 0x30;
      break;
  }
#if !defined ESP32 && !defined ESP8266 && !defined STM32
  serialPort.serial->begin(baud, config);
#else
  serialPort.serial->begin(baud);
#endif
}

static void hardware_serial_init_port(struct serial_port_config *config)
{
  log_debug(F("hardware_serial_init_port"));
  serialPort.setup = hardware_serial_setup;
  serialPort.txchar = hardware_serial_txchar;
  serialPort.start_tx = hardware_serial_start_tx;
  serialPort.stop_tx = hardware_serial_stop_tx;
  serialPort.serial = (HardwareSerial *)config->serial;
}

static void hardware_serial_txchar(uint8_t c)
{
  put_char_in_circbuf(&serialPort.outq, c);
  hardware_serial_start_tx();
}

static void hardware_serial_start_tx(void)
{
  if (uart_circ_empty(&serialPort.outq)) {
    return;
  }
  while (uart_circ_chars_pending(&serialPort.outq) > 0) {
    char c = get_char_from_circbuf(&serialPort.outq);
    serialPort.serial->write(c);
  }
}

static void hardware_serial_stop_tx(void)
{
}

static int hardware_serial_init(void)
{
  if (serialConfig->type == SERIAL_HW) {
    serialPort.type = SERIAL_HW;
    hardware_serial_init_port(serialConfig);
  }
  return 0;
}

void serialEvent(void)
{
  if (Serial.available()) {
    char c = (char)Serial.read();
    sdltp_rx_interrupt(c);
  }
#if defined(HAVE_HWSERIAL1)
  if (Serial2.available()) {
    int c = (char)Serial1.read();
    if (c != -1) {
      sdltp_rx_interrupt(c);
    }
  }
#endif
#if defined(HAVE_HWSERIAL2)
  if (Serial2.available()) {
    int c = (char)Serial2.read();
    if (c != -1) {
      sdltp_rx_interrupt(c);
    }
  }
#endif
#if defined(HAVE_HWSERIAL3)
  if (Serial3.available()) {
    int c = (char)Serial3.read();
    if (c != -1) {
      sdltp_rx_interrupt(c);
    }
  }
#endif
}

static int software_serial_setup(char* options, int flags)
{
  long baud = 9600;
  int bits = 8;
  int parity = 'n';
  int flow = 'n';
  int stops = 1;

  uart_parse_options(options, &baud, &parity, &bits, &flow, &stops);
  log_debug(F("software_serial_setup: %d"), baud);
  serialPort.swserial->begin(baud);
  return 0;
}

static void software_serial_init_port(struct serial_port_config *config)
{
  log_debug(F("software_serial_init_port"));
  serialPort.setup = software_serial_setup;
  serialPort.txchar = software_serial_txchar;
  serialPort.start_tx = software_serial_start_tx;
  serialPort.stop_tx = software_serial_stop_tx;
  serialPort.swserial = (SoftwareSerial *)config->serial;
}

static void software_serial_txchar(uint8_t c)
{
  put_char_in_circbuf(&serialPort.outq, c);
  software_serial_start_tx();
}

static void software_serial_start_tx(void)
{
  if (uart_circ_empty(&serialPort.outq)) {
    return;
  }
  while (uart_circ_chars_pending(&serialPort.outq) > 0) {
    char c = get_char_from_circbuf(&serialPort.outq);
    serialPort.swserial->write(c);
  }
}

static void software_serial_stop_tx(void)
{
}

static int software_serial_init(void)
{
  if (serialConfig->type == SERIAL_SW) {
    serialPort.type = SERIAL_SW;
    software_serial_init_port(serialConfig);
  }
  return 0;
}

void softwareSerialEvent()
{
  if (serialPort.type == SERIAL_SW) {
    if (serialPort.swserial->available()) {
      int c = serialPort.swserial->read();
      sdltp_rx_interrupt(c);
    }
  }
}

static void serial_polling(void)
{
  serialEvent();
  softwareSerialEvent();
}

int serial_driver_init(struct serial_port_config *config)
{
  serialConfig = config;
  hardware_serial_init();
  software_serial_init();
  serial_set_polling(serial_polling);
  return 0;
}

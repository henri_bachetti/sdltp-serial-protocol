
#ifndef _ARDUINO_SERIAL_H_
#define _ARDUINO_SERIAL_H_

struct serial_port_config
{
  int type;
  Stream *serial;
};

extern "C" {
void serialEvent(void);
}
void softwareSerialEvent(void);

#endif



#include <errno.h>

#include "config.h"

#ifdef USE_SDLTP

#include "protocol.h"
#include "sdltp.h"

#define send_ack(seq)					send_a_char(ACK, seq)
#define drop_packet(seq)			send_a_char(ACK, seq)
#define send_nak()						send_a_char(NAK, 0)
#define send_wak()						send_a_char(WAK, 0)

static int check_sequence(void);

static void send_a_char(int c, int nseq)
{
	put_char_in_circbuf(&serialPort.outq, c);
	if (c == ACK) {
		put_char_in_circbuf(&serialPort.outq, nseq);
	}
	serialPort.start_tx();
}

void sdltp_rx_interrupt(int c)
{
	c &= TTY_CHARMASK;
	
	switch (softc.sc_context) {
	case SC_IDLE:
		switch(softc.sc_state) {
		case SC_IDLE:
			if (c == STX) {
				// start single frame reception
				softc.sc_context = SC_RX_FRAME;
				sdltp_rx_frame(c);
			}
			break;
		default:
			break;
		}
		break;
	case SC_RX_FRAME:
	case SC_RX_FRAMES:
	case SC_RX_UNEXPECTED_FRAME:
		sdltp_rx_frame(c);
		break;
	case SC_XMIT_FRAME:
		sdltp_rx_ack(c);
		break;
	default:
		break;
	}
}

void sdltp_rx_frame(int c)
{
	register struct sdltp_softc *sc = (struct sdltp_softc *)serialPort.sc;
	
	mod_timer(&sdltp_char_timer, jiffies + CHAR_TIMEOUT);
	
	switch(softc.sc_state) {
	case SC_IDLE:
		if (c == STX) {
			// start frame reception
			softc.sc_state = SC_START_RECEIVED;
		}
		break;
	case SC_START_RECEIVED:
		// sequence number
		softc.rcv_seq = (uint8_t)c;
		softc.sc_error = 0;
		softc.sc_errcnt = 0;
		softc.buf_len = 0;
		softc.crc = 0;
		update_crc(softc.crc, (uint8_t)c);
		softc.sc_state = SC_NSEQ_RECEIVED;
		break;
	case SC_NSEQ_RECEIVED:
		// data
		switch(c) {
		case DLE:
			update_crc(softc.crc, (uint8_t)c);
			softc.sc_state = SC_DLE_RECEIVED;
			break;
		case ETX:
			// end of frame
			update_crc(softc.crc, (uint8_t)c);
			softc.sc_state = SC_STOP_RECEIVED;
			break;
		default:
			update_crc(softc.crc, (uint8_t)c);
		
			if (softc.buf_len < SDLTP_MTU) {
				softc.buf[softc.buf_len++] = (uint8_t)c;
			}
			else {
				softc.sc_error = E2BIG;
			}
			break;
		}
		break;
	case SC_DLE_RECEIVED:
		// data following DLE
		update_crc(softc.crc, (uint8_t)c);
		softc.sc_state = SC_NSEQ_RECEIVED;
		
		if (softc.buf_len < SDLTP_MTU) {
			softc.buf[softc.buf_len++] = (uint8_t)c;
		}
		else {
			softc.sc_error = E2BIG;
		}
		break;
	case SC_STOP_RECEIVED:
		// CRC (LSB)
		softc.rcv_crc = (uint16_t)c & 0x00FF;
		softc.sc_state = SC_CRC_RECEIVED;
		break;
	case SC_CRC_RECEIVED:
		// CRC (MSB)
		softc.rcv_crc |= ((uint16_t)c << 8) & 0xFF00;
		softc.sc_state = SC_FRAME_RECEIVED;	
	  if(softc.rcv_crc != softc.crc ) {
			log_error(F("invalid CRC %x instead of %x"), softc.rcv_crc, softc.crc);
			softc.sc_error = EINVAL;
		}
		break;
	default:
		break;
	}
	if (softc.sc_state == SC_FRAME_RECEIVED) {
		// data frame complete
		mod_timer(&sdltp_char_timer, 0);
		switch (softc.sc_context) {
		case SC_RX_FRAME:
			sdltp_frame2q();
			break;
		case SC_RX_FRAMES:
			if (softc.rcv_packet == 0) {
				// lets see if there is a list of requests to accept
				// either accept all
				if (softc.accrq.rqlist) {
					int found = 0;
					int n;

					for (n = 0 ; n < strlen(softc.accrq.rqlist) ; n++) {
						if (softc.buf[softc.accrq.offset] == softc.accrq.rqlist[n]) {
							found = 1;
						}
					}
					if (found == 0) {
						softc.sc_context = SC_RX_FRAME;
						mod_timer(&sdltp_frame_timer, 0);
						sdltp_frame2q();
						return;
					}
				}
			}
			// data frame
			sdltp_packet2q();
			// load inter-frame timer
			mod_timer(&sdltp_frame_timer, jiffies + FRAME_TIMEOUT);
			break;
		case SC_RX_UNEXPECTED_FRAME:
			// data frame instead of EOT
			softc.sc_error = EINVAL;
			send_ack(softc.rcv_seq);
			break;
		default:
			break;
		}
	}
}

void sdltp_rx_ack(int c)
{
	register struct sdltp_softc *sc = (struct sdltp_softc *)serialPort.sc;
	
	switch(softc.sc_state) {
	case SC_WAITING_FOR_ACK:
	case SC_WAK_RECEIVED:
		softc.sc_error = 0;
		switch (c) {
		case ACK:
			softc.sc_state = SC_WAITING_FOR_SEQ;
			break;
		case WAK:
			softc.sc_state = SC_WAK_RECEIVED;
			break;
		case NAK:
			softc.sc_state = SC_NAK_RECEIVED;
			break;
		case STX:
			// start single frame reception
			softc.sc_context = SC_RX_FRAME;
			softc.sc_state = SC_IDLE;
			sdltp_rx_frame(c);
			break;
		default:
			break;
		}
		break;
	case SC_WAITING_FOR_SEQ:
		// sequence number
		softc.sc_state = SC_IDLE;
		if (softc.snd_seq != c) {
			softc.sc_error = EINVAL;
		}
		break;
	default:
		break;
	}
	if (softc.sc_state != SC_WAITING_FOR_ACK && softc.sc_state != SC_WAITING_FOR_SEQ) {
		// frame transmission complete or retry
		if (softc.sc_state == SC_IDLE) {
			if (softc.sc_context == SC_XMIT_FRAME) {
				softc.sc_context = SC_IDLE;
			}
		}
		serialPort.wake_up = 1;
	}
}

void sdltp_frame2q(void)
{
	if (softc.sc_error == 0) {
		if (uart_circ_empty(&serialPort.rawq)) {
			if (check_sequence() == 0) {
				// copy the data in the input port buffer
				put_chars_in_circbuf(&serialPort.rawq, softc.buf, softc.buf_len);
				serialPort.wake_up = 1;
				send_ack(softc.rcv_seq);
			}
			else {
				softc.sc_error = EINVAL;
				drop_packet(softc.rcv_seq);
			}
		}
		else {
			send_wak();
			mod_timer(&sdltp_wak_timer, jiffies + WAK_TIMEOUT);
			return;
		}
	}
	else {
		send_nak();
	}
	softc.sc_context = SC_IDLE;
	softc.sc_state = SC_IDLE;
}

void sdltp_packet2q(void)
{
	if (softc.sc_error == 0) {
		if (check_sequence() == 0) {
			if (softc.rcv_packet == 0) {
				softc.rcv_total_size = softc.rcv_total_packet * softc.buf_len;
			}
			if (uart_circ_chars_free(&serialPort.rawq) > softc.buf_len) {
				// copy the data in the input port buffer
				put_chars_in_circbuf(&serialPort.rawq, softc.buf, softc.buf_len);
				++softc.rcv_packet;
			}
			else {
				send_wak();
				mod_timer(&sdltp_wak_timer, jiffies + WAK_TIMEOUT);
				return;
			}
			send_ack(softc.rcv_seq);
			if (softc.sc_mode & PKT_MODE) {
				serialPort.wake_up = 1;
			}
		}
		else {
			softc.sc_error = EINVAL;
			drop_packet(softc.rcv_seq);
		}
	}
	else {
		send_nak();
	}
	if (softc.rcv_packet == softc.rcv_total_packet) {
		// last packet
		softc.sc_context = SC_RX_EOT;
	}
	softc.sc_state = SC_IDLE;
}

static int check_sequence(void)
{
	int next;
	
	if (softc.sc_context == SC_RX_FRAME) {
		// in single frame mode only
		if (softc.last_rcv_seq == 0xff) {
			// first frame we receive
			softc.last_rcv_seq = softc.rcv_seq;
			return 0;
		}
		if (softc.rcv_seq == 0) {
			// first frame from peripheral
			softc.last_rcv_seq = 0;
			return 0;
		}
		if (softc.rcv_seq == softc.last_rcv_seq) {
			// duplicate frame
			return 1;
		}
		softc.last_rcv_seq = softc.rcv_seq;
		return 0;
	}
	next = softc.last_rcv_seq == 0xff ? 0 : softc.last_rcv_seq + 1;
	softc.last_rcv_seq = softc.rcv_seq;
	if (next == softc.rcv_seq) {
		return 0;
	}
	return 1;
}

void sdltp_char_timeout(unsigned long data)
{
  log_debug(F("sdltp_char_timeout"));
	softc.sc_error = EAGAIN;
	if (softc.sc_context != SC_RX_FRAMES) {
		softc.sc_context = SC_IDLE;
	}
	softc.sc_state = SC_IDLE;
	serialPort.wake_up = 1;
	mod_timer(&sdltp_char_timer, 0);
}

void sdltp_frame_timeout(unsigned long data)
{
	register struct sdltp_softc *sc = (struct sdltp_softc *)data;
	
  log_debug(F("sdltp_frame_timeout"));
	uart_circ_clear(&serialPort.rawq);
	if (++softc.sc_errcnt >= MAX_ERRCNT) {
		softc.sc_error = EAGAIN;
		softc.sc_context = SC_IDLE;
		softc.sc_state = SC_IDLE;
		serialPort.wake_up = 1;
		mod_timer(&sdltp_frame_timer, 0);
	}
	else {
		mod_timer(&sdltp_frame_timer, jiffies + FRAME_TIMEOUT);
	}
}

void sdltp_wak_timeout(unsigned long data)
{
	mod_timer(&sdltp_wak_timer, 0);
	switch (softc.sc_context) {
	case SC_RX_FRAME:
		sdltp_frame2q();
		break;
	default:
		break;
	}
}

#endif


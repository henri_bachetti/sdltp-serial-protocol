
#ifndef HOST_H_
#define HOST_H_

#include <time.h>

#include "server.h"

#define MAX_ECHO						64
#define VER_SIZE          7
#define BUILD_SIZE        12

struct host_return_version
{
  struct host_answer hdr;
  char app_id[VER_SIZE];
  char app_build[BUILD_SIZE];
};

struct host_answer_echo
{
	struct host_answer hdr;
	char s[MAX_ECHO];
};

struct host_set_time
{
  uint8_t id;
  time_t time;
};

struct host_get_time
{
  struct host_answer hdr;
  time_t time;
};

struct host_sleep
{
  uint8_t id;
  uint16_t time;
};

struct host_led
{
  uint8_t id;
  uint8_t state;
};

extern struct request my_requests[];

int host_init(void);
int handle_host_requests(void);

#endif /* HOST_H_ */


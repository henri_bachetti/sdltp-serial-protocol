
#include <Time.h>
#if !defined ESP32 && !defined ESP8266 && !defined STM32
#include <avr/wdt.h>
#endif
#include <serial_core.h>

#include "debug.h"
#include "host.h"

int hostPort;

int request_version(const struct request *rq, const uint8_t *buf, int len)
{
  struct host_return_version answer;

  log_debug(F("request_version"));
  answer.hdr.id = rq->answer_id;
  answer.hdr.status = REQ_ACCEPTED;
  strncpy(answer.app_id, "V1.0", VER_SIZE);
  strncpy(answer.app_build, __DATE__, BUILD_SIZE);
  return serial_write(&answer, sizeof(answer));
}

int request_echo(const struct request *rq, const uint8_t *buf, int len)
{
	struct host_answer_echo answer;

	answer.hdr.id = rq->answer_id;
	answer.hdr.status = REQ_ACCEPTED;
	len = min(len, MAX_ECHO - 1);
	strncpy(answer.s, (char *)buf+1, len);
	answer.s[len] = 0;
	return serial_write(&answer, len + 2);
}

int request_reboot(const struct request *rq, const uint8_t *buf, int len)
{
  struct host_answer answer;

  answer.id = rq->answer_id;
  answer.status = REQ_ACCEPTED;
  log_debug(F("reboot"));
  serial_write(&answer, sizeof(answer));
  delay(100);
#if !defined ESP32 || !defined ESP8266 || !defined STM32
  wdt_enable(WDTO_15MS);
#endif
  while (1)
  {
  }
  return 0;
}

static int request_poweroff(const struct request *rq, const uint8_t *buf, int len)
{
  struct host_answer answer;

  answer.id = rq->answer_id;
  answer.status = REQ_ACCEPTED;
  log_debug(F("power off"));
  serial_write(&answer, sizeof(answer));
  delay(100);
  return 0;
}

int request_settime(const struct request *rq, const uint8_t *buf, int len)
{
  struct host_set_time *req = (struct host_set_time *)buf;
  struct host_answer answer;

  answer.id = rq->answer_id;
  answer.status = REQ_ACCEPTED;
  if (len != sizeof(struct host_set_time)) {
    log_error(F("invalid args"));
    answer.status = REQ_REFUSED;
    goto xcpt;
  }
  setTime(req->time);
xcpt:
  return serial_write(&answer, sizeof(answer));
}

int request_gettime(const struct request *rq, const uint8_t *buf, int len)
{
  struct host_get_time answer;

  log_debug(F("request_gettime"));
  answer.hdr.id = rq->answer_id;
  answer.hdr.status = REQ_ACCEPTED;
  answer.time = now();
  log_debug(F("request_gettime %lu"), answer.time);
  return serial_write(&answer, sizeof(answer));
}

int request_sleep(const struct request *rq, const uint8_t *buf, int len)
{
  struct host_sleep *req = (struct host_sleep *)buf;
  struct host_answer answer;

  answer.id = rq->answer_id;
  answer.status = REQ_ACCEPTED;
  if (len != sizeof(struct host_sleep)) {
    log_error(F("invalid args"));
    answer.status = REQ_REFUSED;
    goto xcpt;
  }
xcpt:
  log_debug(F("request_sleep %d"), req->time);
  serial_write(&answer, sizeof(answer));
  delay(req->time);
  return 0;
}

int request_led(const struct request *rq, const uint8_t *buf, int len)
{
  struct host_led *req = (struct host_led *)buf;
  struct host_answer answer;

  answer.id = rq->answer_id;
  answer.status = REQ_ACCEPTED;
  if (len != sizeof(struct host_led)) {
    log_error(F("invalid args"));
    answer.status = REQ_REFUSED;
    goto xcpt;
  }
xcpt:
  log_debug(F("request_led %d"), req->state);
  serial_write(&answer, sizeof(answer));
  pinMode(13, OUTPUT);
  digitalWrite(13, req->state);
  return 0;
}

struct request my_requests[] =
{
  'V', 'v', request_version,
	'!', '!', request_echo,
  'R', 'r', request_reboot,
  'Z', 'z', request_poweroff,
  'U', 'u', request_settime,
  'T', 't', request_gettime,
  'S', 's', request_sleep,
  'L', 'l', request_led,
};

int request_count = sizeof(my_requests) / sizeof(my_requests[0]);

void urh(uint8_t *buf)
{
  struct host_answer answer;

  log_debug(F("unknown"));
  answer.id = buf[0];
  answer.status = REQ_UNKNOWN;
  serial_write(&answer, sizeof(answer));
}

int host_init()
{
  serial_setup(HOST_SETUP, 0);
  log_debug(F("sdltp configured"));
  accept_requests(my_requests, request_count);
  handle_unknown_requests(urh);
  return 0;
}

int handle_host_requests(void)
{
  return (listen2host());
}


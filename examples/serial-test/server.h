
#ifndef SERVER_H_
#define SERVER_H_

#include "sdltp.h"

#define REQ_ACCEPTED					'A'
#define REQ_REFUSED						'R'
#define REQ_BUSY						  'B'
#define REQ_ERROR						  'E'
#define REQ_UNKNOWN						'U'

struct request
{
	char resquest_id;
	char answer_id;
	int (*function)(const struct request *rq, const uint8_t *buf, int len);
};

struct host_answer
{
	uint8_t	id;
	uint8_t status;
};

void accept_requests(const struct request *list, int size);
void handle_unknown_requests(void (*handler)(uint8_t *buf));
int listen2host(void);
int check_request_size(int got, int expected);

#endif /* SERVER_H_ */


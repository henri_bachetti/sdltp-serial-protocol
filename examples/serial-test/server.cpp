
#include "config.h"
#include "console.h"
#include "server.h"

static const struct request *request_list;
static int request_count;
static uint8_t buf[UART_XMIT_SIZE];
static void (*unknown_requests_handler)(uint8_t *buf);

int check_request_size(int got, int expected)
{
  if (got != expected) {
    log_error("invalid args (got %d bytes, expected %d)", got, expected);
    return -1;
  }
  return 0;
}

void accept_requests(const struct request *list, int size)
{
  request_list = list;
  request_count = size;
}

void handle_unknown_requests(void (*handler)(uint8_t *buf))
{
  unknown_requests_handler = handler;
}

int listen2host(void)
{
  int cnt, i, found;
  const struct request *rq;

  cnt = serial_read(buf, UART_XMIT_SIZE);
  if (cnt > 0) {
    buf[cnt] = 0;
    found = 0;
    log_debug(F("listen2host: %c"), buf[0]);
    for (i = 0 ; i < request_count ; i++) {
      rq = request_list + i;
      if (buf[0] == rq->resquest_id) {
        rq->function(rq, buf, cnt);
        log_debug(F("listen2host: OK"));
        found = 1;
      }
    }
    if (!found && unknown_requests_handler) {
      unknown_requests_handler(buf);
    }
  }
  return cnt;
}


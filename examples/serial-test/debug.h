
#include <mpp-console.h>
#include <mpp-logger.h>

#ifndef DEBUG_H_
#define DEBUG_H_

extern Logger LOGGER;

#define log_debug     LOGGER.debug
#define log_error     LOGGER.error

#endif

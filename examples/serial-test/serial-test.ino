
#include <mpp-console.h>
#include <kernel_timers.h>
#include <sdltp.h>

#include "config.h"
#include "host.h"

Logger LOGGER("serial");

#define SW_RX_PIN                 10
#define SW_TX_PIN                 11

SoftwareSerial hostSerial(SW_RX_PIN, SW_TX_PIN);

struct serial_port_config portsConfig = {
//  SERIAL_HW, &Serial2     // work with hardware serial
  SERIAL_SW, &hostSerial  // work with software serial
};

extern uint8_t  *__bss_end;
extern uint8_t  *__heap_end;

void setup()
{
  Serial.begin(115200);
  console_init(&Serial);
  printf("Basic Protocol demo\n");
  LOGGER.init(&Serial);
  LOGGER.setLevel(DEBUG);
  timer_init(1);
  serial_init(&portsConfig, &LOGGER);
  printf("drivers initialization done\n");
  host_init();
  printf("host communication initialization done\n");
}

void loop()
{
  serialEvent();
  softwareSerialEvent();
  handle_host_requests();
}

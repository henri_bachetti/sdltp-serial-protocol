
#include <mpp-console.h>
#include <kernel_timers.h>
#include <sdltp.h>

#include "config.h"

#define SERIAL_SETUP              "230400n8n1"
#define DATA_SIZE                 25

struct packet
{
  uint8_t d8[DATA_SIZE];
  uint16_t d16[DATA_SIZE];
};

Logger LOGGER("receiver");

#define SW_RX_PIN                 10
#define SW_TX_PIN                 11

SoftwareSerial mySerial(SW_RX_PIN, SW_TX_PIN);

struct serial_port_config portsConfig = {
  SERIAL_HW, &Serial2     // work with hardware serial
  //SERIAL_SW, &mySerial  // work with software serial
};

void setup()
{
  Serial.begin(115200);
  console_init(&Serial);
  printf("Basic Emitter demo\n");
  LOGGER.init(&Serial);
  LOGGER.setLevel(DEBUG);
  timer_init(1);
  serial_init(&portsConfig, &LOGGER);
  printf("drivers initialization done\n");
  serial_setup(SERIAL_SETUP, 0);
  log_debug(F("sdltp configured"));
  return 0;
}

void loop()
{
  struct packet pkt;
  int cnt, i, found;

  serialEvent();
  softwareSerialEvent();
  for (int i = 0 ; i < DATA_SIZE ; i++) {
    pkt.d8[i] = i;
  }
  for (int i = 0 ; i < DATA_SIZE ; i++) {
    pkt.d16[i] = 256+i;
  }
  cnt = serial_write(&pkt, sizeof(pkt));
  Serial.print(cnt); Serial.println(" bytes transmitted");
  delay(4000);
}

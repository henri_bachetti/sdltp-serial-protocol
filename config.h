
#ifndef CONFIG_H_
#define CONFIG_H_

// no kernel for this moment
#undef USE_SYSCALLS

// timer A
#define TIMER							TIMER1
#define TICK							5
#define NOMINAL_HZ			            20
#define JIFFY							100
#define HZ                              200

// serial buffer size (must be 4, 8, 16, 32, 64, 128 or 256 bytes)
#define UART_XMIT_SIZE					128

// serial ports
#define HOST_SETUP						"9600n8n1"

// sdltp: maximum transmission unit.
#define USE_SDLTP
#define SDLTP_MTU						120

#endif


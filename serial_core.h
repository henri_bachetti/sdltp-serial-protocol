
#ifndef SERIAL_CORE_H_
#define SERIAL_CORE_H_

#include <Arduino.h>
#include <HardwareSerial.h>
#include <SoftwareSerial.h>

#include "serial_driver.h"
#include "config.h"
#include "console.h"
#include "circ_buf.h"

#define TTY_CHARMASK        0x00ff  /* Character mask */
#define TTY_QUOTE           0x0100  /* Character quoted */
#define TTY_ERRORMASK       0xff00  /* Error mask */
#define TTY_FE              0x0100  /* Framing error or BREAK condition */
#define TTY_PE              0x0200  /* Parity error */
#define TTY_BE              0x0400  /* BREAK condition */
#define TTY_OE              0x0800  /* Overrun error */

#define ICANON              0000002
#define ECHO                0000010

#define TCIFLUSH            0
#define TCOFLUSH            1
#define TCIOFLUSH           2

#define TIOCPKT             1
#define TIOC_SC_STOREPKT    2
#define TIOCSTATS           3
#define TIOC_SC_ACCEPTPKT   4

#if defined(HAVE_HWSERIAL3)
#define NR_HW_PORTS          3
#elif defined(HAVE_HWSERIAL2)
#define NR_HW_PORTS          2
#else
#define NR_HW_PORTS          1
#endif

#define SERIAL_HW            1
#define SERIAL_SW            2

struct serial_port
{
  int port;
  int type;
  void *sc;
  int wake_up;
  struct circ_buf rawq;
  struct circ_buf outq;
  HardwareSerial *serial;
  SoftwareSerial *swserial;
  struct serial_line_discipline *ldisc;
  int (*setup)(char* options, int flags);
  void (*txchar)(uint8_t c);
  void (*start_tx)(void);
  void (*stop_tx)(void);
};

struct serial_accrq
{
  int offset;                       // request offset
  char *rqlist;
};

#define uart_circ_empty(circ)   ((circ)->head == (circ)->tail)
#define uart_circ_clear(circ)   ((circ)->head = (circ)->tail = 0)

#define uart_circ_chars_pending(circ) \
  (CIRC_CNT((circ)->head, (circ)->tail, UART_XMIT_SIZE))

#define uart_circ_chars_free(circ)  \
  (CIRC_SPACE((circ)->head, (circ)->tail, UART_XMIT_SIZE))

static inline void put_char_in_circbuf(struct circ_buf *circ, unsigned char c)
{
  if (!circ->buf) {
  log_debug(F("no circ_buf\n"));
    return;
  }
  while (!uart_circ_chars_free(circ));
  noInterrupts();
  circ->buf[circ->head] = c;
  circ->head = (circ->head + 1) & (UART_XMIT_SIZE - 1);
  interrupts();
}

static inline int put_chars_in_circbuf(struct circ_buf *circ, uint8_t *buf, int size)
{
  int count = 0;

  if (!circ->buf) {
    return 0;
  }
  noInterrupts();
  while (count < size && uart_circ_chars_free(circ)) {
    circ->buf[circ->head] = buf[count++];
    circ->head = (circ->head + 1) & (UART_XMIT_SIZE - 1);
  }
  interrupts();
  return count;
}

static inline void rm_char_from_circbuf(struct circ_buf *circ)
{
  if (!circ->buf)
    return;

  noInterrupts();
  circ->head = circ->head - 1;
  if (circ->head == circ->tail) {
    return;
  }
  if (circ->head < 0 ) {
    circ->head = UART_XMIT_SIZE - 1;
  }
  interrupts();
}

static inline unsigned char get_char_from_circbuf(struct circ_buf* circ)
{
  noInterrupts();
  unsigned char c = circ->buf[ circ->tail ];
  circ->tail = (circ->tail + 1) & (UART_XMIT_SIZE - 1);
  interrupts();
  return c;
}

static inline int get_chars_from_circbuf(struct circ_buf* circ, uint8_t *buf, int size)
{
  int count = 0;
  noInterrupts();
  while (count < size && ! uart_circ_empty(circ)) {
    buf[count++] = circ->buf[ circ->tail ];
    circ->tail = (circ->tail + 1) & (UART_XMIT_SIZE - 1);
  }
  interrupts();
  return count;
}

extern struct serial_port serialPort;

void uart_parse_options(char *options, long *baud, int *parity, int *bits, int *flow, int *stops);
int serial_init(struct serial_port_config *config, Logger *logger);
int serial_driver_init(struct serial_port_config *config);
int serial_setup(char *options, int flags);
int serial_read(void *buf, int size);
int serial_write(void *buf, int size);
int serial_attach(int ldisc);
int serial_flush(int how);
int serial_drain(int hport);
void serial_set_polling(void (*poll)(void));
void serial_poll(void);

#endif


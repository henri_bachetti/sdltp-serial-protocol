
#include "console.h"
#include "serial_core.h"
#include "sdltp.h"

Logger *serial_logger;

static uint8_t  outq_buf[UART_XMIT_SIZE];
static uint8_t  rawq_buf[UART_XMIT_SIZE];

struct serial_port  serialPort;

void (*serialPolling)(void);

void uart_parse_options(char *options, long *baud, int *parity, int *bits, int *flow, int *stops)
{
  char *s = options;

  *baud = strtoul(s, 0, 10);
  while (*s >= '0' && *s <= '9')
    s++;
  if (*s)
    *parity = *s++;
  if (*s)
    *bits = *s++ - '0';
  if (*s)
    *flow = *s++;
  if (*s)
    *stops = *s - '0';
}

int serial_setup(char *options, int flags)
{
  log_debug(F("serial_setup: %s"), options);
  int error = serialPort.setup(options, flags);
  if (error == 0) {
    return sdltp_setup();
  }
  return -1;
}

int serial_read(void *buf, int size)
{
  unsigned int in;
  int error = sdltp_receive(buf, size, &in);
  if (error == 0) {
    return in;
  }
  return -1;
}

int serial_write(void *buf, int size)
{
  unsigned int out = 0;
  int error = sdltp_send(buf, size, &out);
  if (error == 0) {
    return out;
  }
  return -1;
}

int serial_flush(int how)
{
  switch (how) {
    case TCIFLUSH:
      uart_circ_clear(&serialPort.rawq);
      break;
    case TCOFLUSH:
      uart_circ_clear(&serialPort.outq);
      break;
    case TCIOFLUSH:
      uart_circ_clear(&serialPort.rawq);
      uart_circ_clear(&serialPort.outq);
      break;
  }
  return 0;
}

int serial_drain(void)
{
  while (!uart_circ_empty(&serialPort.outq));
  return 0;
}

void serial_set_polling(void (*poll)(void))
{
  serialPolling = poll;
}

void serial_poll(void)
{
  if (serialPolling) {
    serialPolling();
  }
}

int serial_init(struct serial_port_config *config, Logger *logger)
{
  serial_logger = logger;
  serialPort.outq.buf = (char *)outq_buf;
  serialPort.rawq.buf = (char *)rawq_buf;
  return serial_driver_init(config);
}

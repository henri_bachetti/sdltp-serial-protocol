
#ifndef _PROTOCOL_H_
#define _PROTOCOL_H_

#include "circ_buf.h"
#include "serial_core.h"

#define  EZERO      0
#define  EPERM      1
#define  ENOENT     2
#define  ESRCH      3
#define  EINTR      4
#define  EIO        5
#define  ENXIO      6
#define  E2BIG      7
#define  ENOEXEC    8
#define  EBADF      9
#define  ECHILD    10
#define  EAGAIN    11
#define  ENOMEM    12
#define  EACCES    13
#define  EFAULT    14
#define  ENOTBLK   15
#define  EBUSY     16
#define  EEXIST    17
#define  EXDEV     18
#define  ENODEV    19
#define  ENOTDIR   20
#define  EISDIR    21
#define  EINVAL    22
#define  ENFILE    23
#define  EMFILE    24
#define  ENOTTY    25
#define  ETXTBSY   26
#define  EFBIG     27
#define  ENOSPC    28
#define  ESPIPE    29
#define  EROFS     30
#define  EMLINK    31
#define  EPIPE     32
#define  EDOM      33
#define  ERANGE    34
#define  EUCLEAN   35
#define  EDEADLOCK 36
#define  EILSEQ    37

#define SOH                   0x01
#define STX                   0x02
#define ETX                   0x03
#define EOT                   0x04
#define ENQ                   0x05
#define ACK                   0x06
#define DLE                   0x10
#define WAK                   0x13
#define NAK                   0x15
#define ETB                   0x17
#define CAN                   0x18

// idle state
#define SC_IDLE               0
// waiting for acknowledge
#define SC_WAITING_FOR_ACK    1
// NACK have been received
#define SC_NAK_RECEIVED       2
// WACK have been received
#define SC_WAK_RECEIVED       3
// STX have been received
#define SC_START_RECEIVED     4
// DLE have been received
#define SC_DLE_RECEIVED       5
// ETX have been received
#define SC_STOP_RECEIVED      6
// CRC have been received
#define SC_CRC_RECEIVED       7
// a complete frame have been received
#define SC_FRAME_RECEIVED     8
#define SC_MAX_STATE          8

#define SC_MAX_NAME           16

#define SC_DATA               char sc_name[SC_MAX_NAME]; \
                              int sc_mode; \
                              int sc_state; \
                              int sc_error; \
                              int sc_errcnt; \
                              struct serial_accrq accrq; \


// line discipline basic structure.
struct sc_data
{
   SC_DATA
};

#define PKT_MODE                 0x01     // packet mode
#define RAW_MODE                 0x02     // raw mode

#endif


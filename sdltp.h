
#ifndef _SDLTP_H_
#define _SDLTP_H_

#include <Arduino.h>

#include <mpp-console.h>
#include <kernel_timers.h>
#include "config.h"
#include "protocol.h"

#define SC_RX_SOH                (SC_MAX_STATE + 1)
#define SC_RX_UNEXPECTED_SOH     (SC_MAX_STATE + 2)
#define SC_RX_FRAME              (SC_MAX_STATE + 3)
#define SC_RX_FRAMES             (SC_MAX_STATE + 4)
#define SC_RX_UNEXPECTED_FRAME   (SC_MAX_STATE + 5)
#define SC_RX_EOT                (SC_MAX_STATE + 6)
#define SC_RX_UNEXPECTED_EOT     (SC_MAX_STATE + 7)
#define SC_XMIT_FRAME            (SC_MAX_STATE + 8)

#define SC_NSEQ_RECEIVED         (SC_MAX_STATE + 9)
#define SC_EOT_FRAME_RECEIVED    (SC_MAX_STATE + 10)
#define SC_WAITING_FOR_SEQ       (SC_MAX_STATE + 11)

extern struct timer_list sdltp_char_timer;
extern struct timer_list sdltp_frame_timer;
extern struct timer_list sdltp_ack_timer;
extern struct timer_list sdltp_wak_timer;

#define CHAR_TIMEOUT					(HZ/2)
#define FRAME_TIMEOUT					(HZ*3)
#define ACK_TIMEOUT						(HZ*4)
#define WAK_TIMEOUT						(HZ*2)

#define MAX_RETRY						  3
#define MAX_ERRCNT						6

struct sdltp_softc
{
	SC_DATA

	int sc_context;                     // context (SOH reception, DATA reception, EOT reception or transmission)

	uint16_t buf_len;                   // count of character in frame buffer to be receive
	uint8_t  buf[SDLTP_MTU];           // input frame buffer
	uint16_t crc;                       // crc
	uint16_t rcv_crc;                   // crc received
	uint32_t qNbLastSequences;          // Number of seq what will be considered like old sequences

	uint8_t  rcv_seq;                   // reception sequence number
	uint8_t  snd_seq;                   // transmission sequence number
	uint8_t  last_rcv_seq;              // last frame ID
	uint16_t rcv_packet;                // number of packet received
	uint16_t rcv_total_packet;          // number of packet STX-ETX to receive
	uint16_t rcv_total_size;            // number of bytes to receive
};

#include <string.h>
static inline void trace(const char *s)
{
	console_puts(s);
}

#define update_crc(crc, c)    crc = (crc >> 4) ^ sdltp_crctbl[(c ^ crc) & 0x0F]; \
                              crc = (crc >> 4) ^ sdltp_crctbl[((c >> 4) ^ crc) & 0x0F];

extern struct sdltp_softc softc;

extern const uint16_t sdltp_crctbl[];

void sdltp_rx_frame(int c);
void sdltp_rx_ack(int c);
void sdltp_frame2q(void);
void sdltp_packet2q(void);

void sdltp_char_timeout(unsigned long data);
void sdltp_frame_timeout(unsigned long data);
void sdltp_wak_timeout(unsigned long data);

// SDLTP C
#define  SDLTP_MAX_SEQ_NUM      255

int sdltp_setup(void);
int sdltp_receive(void *data, unsigned int len, unsigned int *in);
int sdltp_send(const void *data, unsigned int len, unsigned int *out);
void sdltp_send_wak(void);
void sdltp_rx_interrupt(int c);

#endif


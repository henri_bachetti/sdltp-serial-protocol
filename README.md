# SDLTP - Simple Data Link Transparent Protocol

Simple STX-ETX serial protocol

## Purpose

SDLTP is a serial line protocol able to transmit and receive frames from any host or peripheral.
The frames transission is secured by a CRC16, and retries are implemented in case of bad transmission.
It is implemented for the ARDUINO boards.

### INSTALLATION

Just unzip the archive in the folder named "libraries" in your Arduino folder.

You will need the following libraries :

- https://bitbucket.org/henri_bachetti/kernel_timers.git
- https://bitbucket.org/henri_bachetti/mpp-console-logger.git
- https://github.com/PaulStoffregen/SoftwareSerial

The SoftwareSerial library can be installed from the ARDUINO IDE library manager.

### API

The library provides a very simple API :

**int serial_init(struct serial_port_config *config, Logger *logger);**

Creates the serial line(s) with the address of a configuration structure.

See example below.

**int serial_setup(char *options, int flags);**

Initializes a line with speed and serial parameters.

An example here with Serial2 on a MEGA board and 1230400 baud, 8 bits and no parity, 1 stop bit.

```
#include <mpp-console.h>
#include <kernel_timers.h>
#include <sdltp.h>

#include "config.h"

#define SERIAL_SETUP              "230400n8n1"

Logger LOGGER("receiver");

#define SW_RX_PIN                 10
#define SW_TX_PIN                 11

SoftwareSerial mySerial(SW_RX_PIN, SW_TX_PIN);

struct serial_port_config portsConfig = {
  SERIAL_HW, &Serial2     // work with hardware serial
  //SERIAL_SW, &mySerial  // work with software serial
};

void setup()
{
  Serial.begin(115200);
  console_init(&Serial);
  printf("Basic Receiver demo\n");
  LOGGER.init(&Serial);
  LOGGER.setLevel(DEBUG);
  timer_init(1);
  serial_init(&portsConfig, &LOGGER);
  printf("drivers initialization done\n");
  serial_setup(SERIAL_SETUP, 0);
  log_debug(F("sdltp configured"));
  return 0;
}
```

**int serial_read(void *buf, int size);**

Receives a message from the serial line. The message can be any string or structure :

```
#define DATA_SIZE                 25

struct packet
{
  uint8_t d8[DATA_SIZE];
  uint16_t d16[DATA_SIZE];
};

void loop()
{
  struct packet pkt;
  int cnt, i, found;

  serialEvent();
  softwareSerialEvent();
  cnt = serial_read(&pkt, sizeof(pkt));
  if (cnt > 0) {
    for (int i = 0 ; i < DATA_SIZE ; i++) {
      Serial.print("d8["); Serial.print(i); Serial.print("]"); Serial.println(pkt.d8[i]); 
    }
    for (int i = 0 ; i < DATA_SIZE ; i++) {
      Serial.print("d16["); Serial.print(i); Serial.print("]"); Serial.println(pkt.d16[i]); 
    }
  }
}
```

**int serial_write(void *buf, int size);**

Sends a message to the serial line. The message can be any string or structure :

```
  cnt = serial_read(&pkt, sizeof(pkt));
  if (cnt > 0) {
    // echo the request to the sender
    return serial_write(&pkt, sizeof(pkt));
  }
```

**int serial_flush(int how);**

This function clears the input or output buffer :

TCIFLUSH : clears input buffer

TCOFLUSH : clears output buffer

TCIOFLUSH : clears input and output buffer

**serialEvent() and softwareSerialEvent()**

As you can see in the example, serialEvent() and softwareSerialEvent() must be called before any message reception.

### EXAMPLES

The library provides two example : serial-test and transmitter / receiver

#### serial-test

A command oriented demo between a PC and an ARDUINO.

ARDUINO side :

The serial-test sketch is loaded into an ARDUINO UNO.

An USB to serial converter must be used :

- UNO : Software Serial with RX=D10 & TX=D11

- MEGA : Hardware Serial2

(see portsConfig in serial-test.ino)

PC side :

The test folder contains a test program written in PYTHON and unittest.

It is a commandline utility :

python test.py CommonTest.testVersion : ask for the version

python test.py CommonTest.testSetTime : to test the set time facility

python test.py CommonTest.testGetTime : to test the get time facility

python test.py CommonTest.testAckTimeout : to test the ACK timeout

python test.py CommonTest.testCharTimeout : to test the character timeout

python test.py CommonTest.testNak : to test the NAK

python test.py DemoTest.testLedOn : to turn the LED on

python test.py DemoTest.testLedOff : to turn the LED off

python test.py : runs all the tests

#### transmitter / receiver

A simple transmission demo between two ARDUINOs:

- UNO : Software Serial with RX=D10 & TX=D11

- MEGA : Hardware Serial2

(see portsConfig in receiver.ino and transmitter.ino)

### BLOG
A description in french here : https://riton-duino.blogspot.com/2019/04/arduino-un-protocole-serie.html



#include <mpp-console.h>
#include <mpp-logger.h>

#ifndef CONSOLE_H_
#define CONSOLE_H_

extern Logger *serial_logger;

#define log_debug     if (serial_logger) serial_logger->debug
#define log_error     if (serial_logger) serial_logger->error

#endif



#ifndef _CRC16_H_
#define _CRC16_H_

uint16_t CrcItt16(uint16_t wInitialValue, uint8_t *bpBuffer, int16_t lSize);
uint16_t CrcIBM16(uint16_t Crc, uint8_t *Buffer, uint8_t Length);

#endif


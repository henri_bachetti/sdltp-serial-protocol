
#include <string.h>
#include <errno.h>

#define PROTO_INTERNAL_STRUCTS
#include "config.h"
#include "serial_core.h"
#include "protocol.h"
#include "sdltp.h"

#define SDLTPNBR_OLD_FRAME  250      // 0 < n < 256 determine window of older seq number

struct timer_list sdltp_char_timer;
struct timer_list sdltp_frame_timer;
struct timer_list sdltp_ack_timer;
struct timer_list sdltp_wak_timer;

const uint16_t sdltp_crctbl[] =    // use to calculate crc value
{
  0x0000, 0x1081, 0x2102, 0x3183,
  0x4204, 0x5285, 0x6306, 0x7387,
  0x8408, 0x9489, 0xA50A, 0xB58B,
  0xC60C, 0xD68D, 0xE70E, 0xF78F
};

struct sdltp_softc softc;

static int wait_for_ack(void)
{
  int error = EAGAIN;

  if (softc.sc_state == SC_IDLE) {
    goto done;
  }
  if (softc.sc_state == SC_WAITING_FOR_ACK || softc.sc_state == SC_WAK_RECEIVED) {
    // wait
    mod_timer(&sdltp_ack_timer, jiffies + ACK_TIMEOUT);
    while (sdltp_ack_timer.expires > jiffies) {
      serial_poll();
      if (softc.sc_context != SC_RX_SOH && softc.sc_context != SC_RX_FRAME
          && softc.sc_context != SC_RX_UNEXPECTED_EOT) {
        // same context: tutto va bene
        if (softc.sc_state == SC_IDLE) {
          error = 0;
          break;
        }
        if (softc.sc_state == SC_NAK_RECEIVED) {
          error = EINVAL;
          break;
        }
        else if (softc.sc_state == SC_WAK_RECEIVED) {
          softc.sc_state = SC_WAITING_FOR_ACK;
          error = EBUSY;
        }
      }
      else {
        // a context switch has occured
        error = EINTR;
        break;
      }
    }
  }
  mod_timer(&sdltp_ack_timer, 0);
done:
  return error;
}

void sdltp_send_wak(void)
{
  if (sdltp_frame_timer.expires != 0) {
    mod_timer(&sdltp_frame_timer, jiffies + FRAME_TIMEOUT);
  }
  put_char_in_circbuf(&serialPort.outq, WAK);
  serialPort.start_tx();
}

static int makeframe(const void *data, unsigned int len)
{
  const uint8_t *pbBuf = (uint8_t *)data;
  uint16_t crc = 0;
  int cnt;

  if (len > SDLTP_MTU) {
    return EFBIG;
  }
  put_char_in_circbuf(&serialPort.outq, STX);
  update_crc(crc, (uint8_t)softc.snd_seq);
  put_char_in_circbuf(&serialPort.outq, softc.snd_seq);

  // escape control characters
  for (cnt = 0 ; cnt < (int)len ; cnt++) {
    switch (pbBuf[cnt]) {
      case STX:
      case ETX:
      case DLE:
      case SOH:
      case EOT:
        update_crc(crc, (uint8_t)DLE);
        put_char_in_circbuf(&serialPort.outq, DLE);
        break;
      default:
        break;
    }
    update_crc(crc, pbBuf[cnt]);
    put_char_in_circbuf(&serialPort.outq, pbBuf[cnt]);
  }

  update_crc(crc, (uint8_t)ETX);
  put_char_in_circbuf(&serialPort.outq, ETX);
  put_char_in_circbuf(&serialPort.outq, (crc & 0x00FF));
  put_char_in_circbuf(&serialPort.outq, (crc & 0xFF00) >> 8);
  return 0;
}

static int send(const void *data, unsigned int len, unsigned int *out, int flags)
{
  int error = 0;
  int retry;

  if (out != 0) {
    *out = 0;
  }
  if (softc.sc_state != SC_IDLE) {
    return EBUSY;
  }
  for (retry = 0 ; retry < MAX_RETRY ; retry++) {
    softc.sc_state = SC_WAITING_FOR_ACK;
    /* make frame */
    error = makeframe(data, len);
    if (error != 0) {
      return error;
    }

    /* send frame */
    serialPort.start_tx();

    do {
      error = wait_for_ack();
      if (error == EINTR) {
        goto done;
      }
    } while (error == EBUSY);

    if (error == 0) {
      if (out != 0) {
        *out = len;
      }
      break;
    }
    else {
      if (error == EBUSY) {
        return error;
      }
    }
    log_debug(F("retry %d"), retry+1);
  }
done:
  softc.sc_state = SC_IDLE;
  return error;
}

int sdltp_setup(void)
{
  serialPort.sc = (void *)&softc;
  softc.sc_error = 0;
  softc.sc_mode = 0;
  softc.sc_state = SC_IDLE;

  log_debug(F("sdltp_setup"));
  init_timer(&sdltp_char_timer);
  init_timer(&sdltp_frame_timer);
  init_timer(&sdltp_ack_timer);
  init_timer(&sdltp_wak_timer);
  add_timer(&sdltp_char_timer);
  add_timer(&sdltp_frame_timer);
  add_timer(&sdltp_ack_timer);
  add_timer(&sdltp_wak_timer);
  sdltp_char_timer.function = sdltp_char_timeout;
  sdltp_frame_timer.function = sdltp_frame_timeout;
  sdltp_wak_timer.function = sdltp_wak_timeout;
  sdltp_char_timer.data = (unsigned long)&softc;
  sdltp_frame_timer.data = (unsigned long)&softc;
  sdltp_wak_timer.data = (unsigned long)&softc;
  serial_flush(TCIOFLUSH);
  softc.sc_context = SC_IDLE;
  return 0;
}

int sdltp_receive(void *data, unsigned int len, unsigned int *in)
{
  int error = 0;

  *in = 0;
  while (1) {
    if (uart_circ_chars_pending(&serialPort.rawq) != 0) {
      // return only data in the following cases:
      // - single packet reception
      // - multiple packet reception && packet mode is ON
      if (softc.sc_context == SC_IDLE || softc.sc_context == SC_RX_FRAME
          || ((softc.sc_context == SC_RX_FRAMES || softc.sc_context == SC_RX_EOT)
              && softc.sc_mode & PKT_MODE)) {
        // input queue not empty -> return data
        *in = get_chars_from_circbuf(&serialPort.rawq, (uint8_t *)data, len);
        error = 0;
        goto done;
      }
    }
    return -1;
  }
done:
  return error;
}

int sdltp_send_frames(const void *data, unsigned int len, unsigned int *out)
{
  const uint8_t *pbBuf = (uint8_t *)data;
  int n;
  unsigned int sent;
  int error = 0;
  int npackets;
  int remainder;

  if (len <= SDLTP_MTU) {
    // data size < packet size -> send 1 frame
    softc.sc_context = SC_XMIT_FRAME;
    error = send(pbBuf, len, &sent, 0);
    softc.snd_seq++;
    if (error) {
      softc.sc_context = SC_IDLE;
      return error;
    }
    *out = sent;
  }
  return error;
}

int sdltp_send(const void *data, unsigned int len, unsigned int *out)
{
  *out = 0;
  if (softc.sc_context != SC_IDLE) {
    // wait until the device are available
    return EBUSY;
  }
  return sdltp_send_frames(data, len, out);
}
